#!/bin/bash - 
#===============================================================================
#
#          FILE: flash_on_sdcard.sh
# 
#         USAGE: ./flash_on_sdcard.sh 
# 
#   DESCRIPTION: Flashs a raspios on a ssd-card 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: 
#       CREATED: 08/04/2020 09:45:24 PM
#       VERSION:  1.0
#===============================================================================

# List the available Devices
echo ""
echo "List of devices:"
lsblk

# Choosing a device
echo ""
echo "Enter the full path of the device you want to use: e. g.: /dev/sda"
read devpath

# Choosing an image
echo ""
echo "List of existing images:"
ls $HOME/Downloads/OS/raspios/ | grep .img
echo ""
echo "Choose the image you want to flash: e. g. 2020-05-27-raspios-buster-lite-armhf.img"
read image

# Flash the .img on the SD-Card
sudo dd if=$HOME/Downloads/OS/raspios/${image} of=${devpath} bs=4M status=progress

