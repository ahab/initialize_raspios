#!/bin/bash -
#===============================================================================
#
#          FILE: addssh_poststartskript.sh
#
#         USAGE: ./addssh_poststartskript.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: mailbox.org
#       CREATED: 10/20/2020 16:50:30 PM
#       VERSION: 1.1
#===============================================================================

# Mount the choosen device on /mnt
#sudo mount "$devpath"1 /mnt
mount_device() {
#if [ "$devpath" == "/dev/sd{?}" ]
#then
    sudo mount /dev/sda"$partition" /mnt #"$devpath""$partition" /mnt
#elif [ "$devpath" == "mmcblk0" ]
#then
#    sudo mount /dev/mmcblk0p"$partition" /mnt
#else
#    echo "No valid path of the device. Exit here."
#    exit 1
#fi
}

# List the available Devices
echo ""
echo "List of devices:"
lsblk
echo ""

# Choosing a device
echo ""
echo "Enter the full path of the device: e. g.: /dev/sda No partition is needed!"
read devpath
partition1=1
sudo mount $devpath$partition1 /mnt
#mount_device

# Add the ssh to partition 1
sudo touch /mnt/ssh

# Umount
sudo umount /mnt

# Inser the post_start.sh to home/pi/
partition2=2
sudo mount $devpath$partition2
#mount_device
#sudo cp post_start.sh /mnt/home/pi/
sudo umount /mnt

