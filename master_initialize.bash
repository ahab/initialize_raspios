#!/bin/bash - 
    #===============================================================================
    #
    #          FILE: master_initialize.sh
    # 
    #         USAGE: ./master_initialize.sh 
    # 
    #   DESCRIPTION: Baut alle noetigen Skripte zusammen, dass nur noch dieses hier benoetigt wird.
    # 
    #       OPTIONS: ---
    #  REQUIREMENTS: ---
    #          BUGS: ---
    #         NOTES: ---
    #        AUTHOR: neuemail@mailbox.org
    #  ORGANIZATION: mailbox.org
    #       CREATED: 22/12/2020 16:50:30 PM
    #       VERSION: 0.2
    #===============================================================================

echo "Auch hier stehen am Anfang einige Fragen: "
echo ""
echo "1. Soll nach einem aktuellen raspbian gesucht werden und das ggf. herunter geladen werden?"
read download_answer
echo ""
echo "2. Moechtest du ein raspbian auf eine SSD-Karte flashen?"
read flash_answer
echo ""
echo "3. Soll SSH enabled werden und das post_start_skript auf die SSD-Karte kopiert werden?"
read ssh_answer
echo ""
echo "Glueckwunsch, das wars schon. Jetzt lass den Computer mal arbeiten."

if [ "$download_answer" == "yes" ]; then
    ./download_latest_raspbian.sh
fi

if [ "$flash_answer" == "yes" ]; then
    ./flash_on_sdcard.sh
fi

if [ "$ssh_answer" == "yes" ]; then
    ./addssh_poststartskript.sh
fi
exit 0

