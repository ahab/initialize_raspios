#!/bin/bash - 
#===============================================================================
#
#          FILE: download_latest_raspbian.sh
# 
#         USAGE: ./download_latest_raspbian.sh 
# 
#   DESCRIPTION: Dieses Skript lädt das aktuellste raspian als torrent herunter und dann als zip via aria2c herunter. Mittels p7zip wird die Datei entpackt und unter $HOME/Downloads/OS/raspios/ abgelegt.
# 
#       OPTIONS: ---
#  REQUIREMENTS: p7zip, aria2c, wget
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: mailbox.org
#       CREATED: 08/04/2020 10:02:57 PM
#       VERSION: 1.4.4
#       RELEASE: 29/12/2020
#===============================================================================

echo "Fuer den korrekten Ablauf werden die Programme aria2c und p7zip benoetigt. Sollten diese nicht vorhanden sein, wird das hier nicht funktionieren."
echo "Moechtest du hier abbrechen und die Programme installieren? yes|no|i for installing it here"
read answer0
if [ "$answer0" == "yes" ]; then
	exit 0
elif [ "$answer0" == "i" ];then
	yay -S aria2c p7zip
fi

# Create a new folder $HOME/Downloads/OS/raspios/
if ! [ -d $HOME/Downloads/OS/raspios/ ]; then
	mkdir -p $HOME/Downloads/OS/raspios/

# Download the latest raspios-torrent
	cd $HOME/Downloads/OS/raspios/
	wget https://downloads.raspberrypi.org/raspios_lite_armhf_latest.torrent

else
    cd $HOME/Downloads/OS/raspios/
    echo ""
    ls
    echo ""
    echo "Is there a OS-file new enough?yes or no"
    read answer
    if [ "$answer" == "yes" ]; then
	echo "Dann sind wir hier fertig."
	exit 0
    elif [ "$answer" == "no" ]; then
    wget https://downloads.raspberrypi.org/raspios_lite_armhf_latest.torrent
    fi
fi
cd $HOME/Downloads/OS/raspios/

# Check if there is the aria2c-tool and if so, download the img-file
if [ -f /bin/aria2c ]; then
	if [ -e raspios_lite_armhf_latest.torrent ]; then
	    aria2c --seed-time 0.1 raspios_lite_armhf_latest.torrent > tmp
	    rm raspios_lite_armhf_latest.torrent
	else
	    echo "Sorry, da ist was schief gegangen, kein torrent vorhanden."
	    exit 1
	fi
else
	echo "Kein aria2c -> Bitte erschiesse deinen PC"
	exit 1
fi

# Extract the zip-file
if [ -f /bin/7z ]; then
	if [ -e tmp ]; then
	    tmp2=$(tail -n4 tmp | head -n1)
	    7z e $(echo ${tmp2:24})
	    rm tmp
	else
	    echo "Leider keine zip-Datei vorhanden"
	    exit 1
	fi
else
	echo "Kein p7zip -> Bitte erschiesse deinen PC"
fi
exit 0

