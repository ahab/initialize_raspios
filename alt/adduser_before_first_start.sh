#!/bin/bash - 
#===============================================================================
#
#          FILE: adduser_before_first_start.sh
# 
#         USAGE: ./adduser_before_first_start.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: mailbox.org
#       CREATED: 08/04/2020 10:09:13 PM
#       VERSION: 1.0
#===============================================================================

# List the available Devices
echo ""
echo "List of devices:"
lsblk

# Choosing a device
echo ""
echo "Enter the full path of the device: e. g.: /dev/sda No partition is needed!"
read devpath

# Mount the choosen device on /mnt
sudo mount "$devpath"p1 /mnt

# Add the ssh to partition 1
sudo touch ssh /mnt/

# Umount
sudo umount /mnt

# Mount the second partition
sudo mount "$devpath"p2 /mnt

# Creating a new user:
echo "Input the name of the user you want to add: "
read NEWUSER

sudo chmod o+w /mnt/etc/passwd
echo "${NEWUSER}:x:1001:1001:,,,:/home/${NEWUSER}:/bin/bash" >> /mnt/etc/passwd
sudo chmod o-w /mnt/etc/passwd

sudo chmod o+w /mnt/etc/shadow
echo "${NEWUSER}:*:18400:0:99999:7:::" >> /mnt/etc/shadow
sudo chmod o-w /mnt/etc/shadow

# Creating a new group (myssh) and adding the new user on it:
sudo chmod o+w /mnt/etc/group
echo "myssh:x:1111:${NEWUSER}" >> /mnt/etc/group
sudo chmod o-w /mnt/etc/group

Deleting user pi
adding user $NEWUSER to Group sudo
Adding the .bash_aliases
Adding the /etc/ssh/sshd_config

