#!/bin/bash - 
#===============================================================================
#
#          FILE: flash_saved_img.sh
# 
#         USAGE: ./flash_saved_img.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: 
#       CREATED: 08/02/2020 01:54:55 PM
#      REVISION:  ---
#===============================================================================
blocksize="4M"
echo "Enter the name of the image (It must be located under ~/Downloads/OS/):"
read img
echo "Enter where you want to flash the image (e. g.: /dev/sda)
read destination
gunzip -c ~/Downloads/OS/"$img" | sudo dd of="$destination" bs="$blocksize" status=progress
