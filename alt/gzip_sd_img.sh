#!/bin/bash - 
#===============================================================================
#
#          FILE: save_pi_img.sh
# 
#         USAGE:  
# 
#   DESCRIPTION: Mit diesem Script wird der Inhalt einer SD-Karte mittels dem Befehl dd und gzip auf den Rechner gespeichert.
# 
#       OPTIONS: ---
#  REQUIREMENTS: gzip
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail+save_pi@mailbox.org 
#       VERSION: 1.0
#  ORGANIZATION: 
#       CREATED: 08/02/2020 01:34:25 PM
#      REVISION:  ---
#===============================================================================

blocksize="1M"
destination="~/Downloads/OS/"

echo "Enter the path of the input-file (e. g. /dev/sda ):"
read input_file
echo "Enter the blocksize. Enter for default one: 1M"
read blocksize
echo "Enter the destination of the saved image (e. g. ~/Downloads/ ):"
echo "Press enter for default one: ~/Downloads/OS/"
read destination
echo "Enter the name of the image:"
read img_name

sudo dd if=$input_file bs=$blocksize | gzip -c > ~/$destination/$img_name

