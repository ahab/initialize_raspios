#!/bin/bash - 
#===============================================================================
#
#          FILE: cp_nec_files.sh
# 
#         USAGE: ./cp_nec_files.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: mailbox.org
#       CREATED: 08/04/2020 10:09:13 PM
#       VERSION: 1.3
#===============================================================================

# Check partition and mount the given one.
echo "You should use partition 2."
./choose_device.sh

# Import the sshd_config
sudo cp sshd_config /mnt/home/pi/

# Import the bash_aliases
sudo cp bash_aliases /mnt/home/pi/.bash_aliases
#sudo cp bash_aliases /mnt/etc/skel/.bash_aliases
sudo cp authorized_keys /mnt/home/pi/authorized_keys

# Import the post start script
sudo cp post_start.sh /mnt/home/pi/post_start.sh

# Creating a new group (myssh) and adding the new user on it:
sudo chmod o+w /mnt/etc/group
echo "myssh:x:1111:pi" >> /mnt/etc/group
sudo chmod o-w /mnt/etc/group

# Umount
sudo umount /mnt
