#!/bin/bash - 
#===============================================================================
#
#          FILE: addssh.sh
# 
#         USAGE: ./addssh.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: neuemail@mailbox.org
#  ORGANIZATION: mailbox.org
#       CREATED: 08/04/2020 10:09:13 PM
#       VERSION: 1.0
#===============================================================================

# List the available Devices
echo ""
echo "List of devices:"
lsblk

# Choosing a device
echo ""
echo "Enter the full path of the device: e. g.: /dev/sda No partition is needed!"
read devpath

# Mount the choosen device on /mnt
sudo mount "$devpath"p1 /mnt

#if [ "$devpath" == "sd{?}" ]
#then
#    sudo mount "$devpath"1 /mnt
#elif [ "$devpath" == "mmcblk0" ]
#then
#    sudo mount "$devpath"p1 /mnt
#else
#    echo "No valid path of the device. Exit here."
#    exit 1
#fi

# Add the ssh to partition 1
sudo touch /mnt/ssh

# Umount
sudo umount /mnt

